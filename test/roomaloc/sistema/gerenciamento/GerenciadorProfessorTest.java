/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import roomaloc.sistema.Professor;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorProfessorTest {
    
    public GerenciadorProfessorTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of guardar method, of class GerenciadorProfessor.
     */
    @Test
    public void testGuardar() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        Professor prof = new Professor(login, senha, mat);
        prof.setUndAcademica(undAcad);
        GerenciadorProfessor ins = new GerenciadorProfessor();
        ins.guardar(prof);
    }

    /**
     * Test of excluir method, of class GerenciadorProfessor.
     */
    @Test
    public void testExcluir() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        Professor prof = new Professor(login, senha, mat);
        prof.setUndAcademica(undAcad);
        GerenciadorProfessor ins = new GerenciadorProfessor();
        ins.guardar(prof);
        ins.excluir(login);
        assertNull(ins.localiza(login));
    }

    /**
     * Test of contain method, of class GerenciadorProfessor.
     */
    @Test
    public void testContain() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        Professor prof = new Professor(login, senha, mat);
        prof.setUndAcademica(undAcad);
        GerenciadorProfessor ins = new GerenciadorProfessor();
        ins.guardar(prof);
        assertTrue(ins.contain(prof));
    }

    /**
     * Test of localiza method, of class GerenciadorProfessor.
     */
    @Test
    public void testLocaliza() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        Professor prof = new Professor(login, senha, mat);
        prof.setUndAcademica(undAcad);
        GerenciadorProfessor ins = new GerenciadorProfessor();
        ins.guardar(prof);
        assertEquals(prof, ins.localiza(login));
    }

    /**
     * Test of listar method, of class GerenciadorProfessor.
     */
    @Test
    public void testListar() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        Professor prof = new Professor(login, senha, mat);
        prof.setUndAcademica(undAcad);
        GerenciadorProfessor ins = new GerenciadorProfessor();
        ins.guardar(prof);
        assertNotNull(ins.localiza(login));
    }
    
}
