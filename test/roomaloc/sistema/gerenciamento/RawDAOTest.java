/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.sistema.gerenciamento;

import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
@RunWith(Suite.class)
@Suite.SuiteClasses({roomaloc.sistema.gerenciamento.GerenciadorProfessorTest.class, roomaloc.sistema.gerenciamento.GerenciadorAdministradorTest.class, roomaloc.sistema.gerenciamento.GerenciadorAssistenteSalaTest.class})
public class RawDAOTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }
    
}
