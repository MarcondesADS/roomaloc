/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import roomaloc.sistema.AssistenteSala;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorAssistenteSalaTest {
    
    public GerenciadorAssistenteSalaTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of guardarAssistente method, of class GerenciadorAssistenteSala.
     */
    @Test
    public void testGuardarAssistente() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        AssistenteSala ass = new AssistenteSala(login, senha, mat);
        ass.setUndAcademica(undAcad);
        GerenciadorAssistenteSala ins = new GerenciadorAssistenteSala();
        ins.guardar(ass);
    }

    /**
     * Test of excluirAdm method, of class GerenciadorAssistenteSala.
     */
    @Test
    public void testExcluirAdm() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        AssistenteSala ass = new AssistenteSala(login, senha, mat);
        ass.setUndAcademica(undAcad);
        GerenciadorAssistenteSala ins = new GerenciadorAssistenteSala();
        ins.guardar(ass);
        ins.excluir(login);
        assertNull(ins.localiza(login));
    }

    /**
     * Test of contain method, of class GerenciadorAssistenteSala.
     */
    @Test
    public void testContain() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        AssistenteSala ass = new AssistenteSala(login, senha, mat);
        ass.setUndAcademica(undAcad);
        GerenciadorAssistenteSala ins = new GerenciadorAssistenteSala();
        ins.guardar(ass);
        assertTrue(ins.contain(ass));
    }

    /**
     * Test of localiza method, of class GerenciadorAssistenteSala.
     */
    @Test
    public void testLocaliza() throws Exception {
       String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        AssistenteSala ass = new AssistenteSala(login, senha, mat);
        ass.setUndAcademica(undAcad);
        GerenciadorAssistenteSala ins = new GerenciadorAssistenteSala();
        ins.guardar(ass);
        assertEquals(ass, ins.localiza(login));
    }

    /**
     * Test of listar method, of class GerenciadorAssistenteSala.
     */
    @Test
    public void testListar() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        AssistenteSala ass = new AssistenteSala(login, senha, mat);
        ass.setUndAcademica(undAcad);
        GerenciadorAssistenteSala ins = new GerenciadorAssistenteSala();
        ins.guardar(ass);
        assertNotNull(ins.listar());
    }
    
}
