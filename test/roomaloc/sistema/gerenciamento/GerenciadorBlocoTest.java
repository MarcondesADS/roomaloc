package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.Sala;
import roomaloc.sistema.SalaBuilder;
import roomaloc.sistema.TipoSala;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorBlocoTest {
    
    public GerenciadorBlocoTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of guardarBloco method, of class GerenciadorBloco.
     */
    @Test
    public void testGuardarBloco() throws Exception {
        System.out.println("guardarBloco");
        Bloco bk = new Bloco("Um Nome");
        GerenciadorBloco instance = new GerenciadorBloco();
        instance.guardarBloco(bk);
    }

    /**
     * Test of excluirBloco method, of class GerenciadorBloco.
     */
    @Test
    public void testExcluirBloco() throws Exception {
        System.out.println("excluirSala");
        String sigla = "UN";
        GerenciadorBloco instance = new GerenciadorBloco();
        instance.excluirBloco(sigla);
    }

    /**
     * Test of contain method, of class GerenciadorBloco.
     */
    @Test
    public void testNotContain() throws Exception {
        System.out.println("contain");
        Bloco bk = new Bloco("Um Nome");;
        GerenciadorBloco instance = new GerenciadorBloco();
        boolean expResult = false;
        boolean result = instance.contain(bk);
        assertEquals(expResult, result);
    }

    /**
     * Test of localiza method, of class GerenciadorBloco.
     */
    @Test
    public void testLocaliza() throws Exception {
        System.out.println("localiza");
        Bloco bk = new Bloco("Um Nome");
        String sigla = "UN";
        GerenciadorBloco instance = new GerenciadorBloco();
        instance.guardarBloco(bk);
        Bloco expResult = bk;
        Bloco result = instance.localiza(sigla);
        assertEquals(expResult, result);
    }

    /**
     * Test of listar method, of class GerenciadorBloco.
     */
    @Test
    public void testListar() throws Exception {
        System.out.println("listar");
        Bloco bk = new Bloco("Um Nome");
        GerenciadorBloco instance = new GerenciadorBloco();
        instance.guardarBloco(bk);
        Collection<Bloco> result = instance.listar();
        assertTrue(result.contains(bk));
    }

    /**
     * Test of update method, of class GerenciadorBloco.
     */
    @Test
    public void testUpdate() throws Exception {
        System.out.println("update");
        Bloco bk = new Bloco("Um Nome");
        Sala sl = new SalaBuilder().comId("UN1").comCapacidadeDe(12).doTipo(TipoSala.NORMAL).build();
        GerenciadorBloco instance = new GerenciadorBloco();
        instance.guardarBloco(bk);
        bk.addSala(sl);
        instance.update(bk);
    }
    
}
