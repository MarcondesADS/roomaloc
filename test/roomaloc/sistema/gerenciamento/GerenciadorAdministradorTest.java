package roomaloc.sistema.gerenciamento;

import java.util.Random;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import roomaloc.sistema.Administrador;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorAdministradorTest {
    
    private Administrador adm;
    
    public GerenciadorAdministradorTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of guardarAdm method, of class GerenciadorAdministrador.
     */
    @Test
    public void testGuardarAdm() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        adm = new Administrador(login, senha, mat);
        adm.setUndAcademica(undAcad);
        GerenciadorAdministrador ins = new GerenciadorAdministrador();
        ins.guardarAdm(adm);
    }

    /**
     * Test of excluirAdm method, of class GerenciadorAdministrador.
     */
    @Test
    public void testExcluirAdm() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        adm = new Administrador(login, senha, mat);
        adm.setUndAcademica(undAcad);
        GerenciadorAdministrador ins = new GerenciadorAdministrador();
        ins.guardarAdm(adm);
        ins.excluirAdm(login);
        assertEquals(null, ins.localiza(login));
    }

    /**
     * Test of contain method, of class GerenciadorAdministrador.
     */
    @Test
    public void testContain() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        adm = new Administrador(login, senha, mat);
        adm.setUndAcademica(undAcad);
        GerenciadorAdministrador ins = new GerenciadorAdministrador();
        ins.guardarAdm(adm);
        assertTrue(ins.contain(adm));
    }

    /**
     * Test of localiza method, of class GerenciadorAdministrador.
     */
    @Test
    public void testLocaliza() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        adm = new Administrador(login, senha, mat);
        adm.setUndAcademica(undAcad);
        GerenciadorAdministrador ins = new GerenciadorAdministrador();
        ins.guardarAdm(adm);
        assertEquals(adm, ins.localiza(login));
    }

    /**
     * Test of listar method, of class GerenciadorAdministrador.
     */
    @Test
    public void testListar() throws Exception {
        String login = String.format("%f", new Random().nextDouble());
        String senha = String.format("%f", new Random().nextDouble());
        String mat = String.format("%f", new Random().nextDouble());
        String undAcad = String.format("%f", new Random().nextDouble());
        adm = new Administrador(login, senha, mat);
        adm.setUndAcademica(undAcad);
        GerenciadorAdministrador ins = new GerenciadorAdministrador();
        ins.guardarAdm(adm);
        assertNotNull(ins.listar());
    }
    
}
