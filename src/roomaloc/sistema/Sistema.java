package roomaloc.sistema;

import java.util.ArrayList;
import roomaloc.config.Configuracao;
import java.util.Collection;
import java.util.Collections;
import javax.security.auth.login.AccountException;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.gerenciamento.GerenciadorAdministrador;
import roomaloc.sistema.gerenciamento.GerenciadorAssistenteSala;
import roomaloc.sistema.gerenciamento.GerenciadorProfessor;

/**
 * Classe de controle do sistema, esta classe deve ser criada utilizando o método getInstance(), esta classe
 * configura um usuario padrão de login e senha 'adm' para primeiro login e executa os scripts de inicialização
 * presentes em config.xml.
 * @author José Marcondes do Nascimento Junior
 */
public class Sistema {
    
    private Gerenciador usrLogado;
    private static Sistema instance;
    private Collection<Usuario> usuarios;
    private Collection<Bloco> blocos;
    
    private Sistema(){}
    
    /**
     * Retorna uma instancia de sistema, está instancia é unica.
     * @return Instancia de sistema. 
     */
    public static Sistema getInstance(){
        if(instance == null){
            instance = new Sistema();
            Configuracao.startScript();
        }
        return instance;
    }
      
    /**
     * Metodo para cadastrar um novo usuário no sistema, este usuário deve possuir obrigatoriamente
     * login senha e matrícula válidos.
     * @param usr Usuário que será cadastrador
     * @throws PermissaoException Quando o usuário logado não possui permição para realizar a ação.
     * @throws PersistenceExeception Erro no acesso a base de dados ou caso o usuário não possuia as
     * seguintes especificações: login com mais de 6 caracteres e senha com mais de 6 caracteres.
     * @throws NullPointerException Caso não exista usuario logado no sistema ou caso o usuário para
     * cadastro seja nulo.
     */
    public void cadastrarUsuario(Usuario usr) throws PermissaoException,PersistenceExeception, NullPointerException{
        if(usr.getLogin() == null || usr.getLogin().equals(""))
            throw new PersistenceExeception("Você deve possuir um login válido");
        if(usr.getLogin().length() < 6)
            throw new PersistenceExeception("O login deve ter mais de 6 caracteres");
        if(usr.getLogin().contains(" "))
            throw new PersistenceExeception("O login não pode conter espaços em branco");
        if(usr.getSenha() == null || usr.getSenha().equals("") || usr.getSenha().length() < 6)
            throw new PersistenceExeception("A senha deve ter mais de 6 digitos");
        if(usr.getMatricula() == null || usr.getMatricula().equals(""))
            throw  new PersistenceExeception("Matricula inválida");
        usrLogado.cadastrarUsuario(usr);
        if(usuarios != null)
            usuarios.add(usr);
    }
    
    
    /**
     * Exclui um usuário do sistema.
     * @param login Login do usuário a ser excluido.
     * @throws PermissaoException Caso o usuário logado não possuia permição para realizar esta ação ou caso
     * um administrador tente excluir algum usuário que não seja ele próprio.
     * @throws PersistenceExeception Erro no acesso a base de dados.
     * @throws NullPointerException Quando não existe usuário logado no sistema.
     */
    public void excluirUsuario(String login) throws PermissaoException, PersistenceExeception,NullPointerException{
        
        usrLogado.excluirUsuario(login);
        usuarios.removeIf((Usuario usr) ->{
            return usr.getLogin().equals(login);
        });
        if(login.equals(((Usuario) usrLogado).getLogin()))
            this.logoff();
    }
    
    public Gerenciador getUsrLogado() {
        return usrLogado;
    }

    /**
     * Loga um usuário no sistema.
     * @param login Login do usuário
     * @param senha Senha do usuário
     * @throws PersistenceExeception Erro de acesso a base de dados.
     * @throws AccountException A senha está errada.
     */
    public void login(String login, String senha) throws PersistenceExeception, AccountException{
        GerenciadorAdministrador gA = new GerenciadorAdministrador();
        GerenciadorAssistenteSala gAs = new GerenciadorAssistenteSala();
        GerenciadorProfessor gP = new GerenciadorProfessor();
        Usuario usr = gA.localiza(login);
        if(usr == null)
            usr = gAs.localiza(login);
        if(usr == null)
            usr = gP.localiza(login);
        
        if(usr.getSenha().equals(senha)){
            this.usrLogado = usr;
        }
        else
            throw new AccountException("Senha incorreta");
    }
    
    /**
     * Lista todos os usuário atualmente carregados no sistema.
     * @return Coleção de todos os usuários.
     */
    public Collection<Usuario> listarUsuarios(){
        if(usuarios == null){
            try {
                this.usuarios = this.usrLogado.listarUsuarios();
            } catch (PermissaoException | PersistenceExeception ex) {
                this.usuarios = Collections.emptyList();
            }
        }
        return Collections.unmodifiableCollection(usuarios);
    }
    
    public void cadastrarBloco(Bloco bk) throws PermissaoException, PersistenceExeception, SalaInvalida{
        usrLogado.cadastrarBloco(bk);
        if(blocos == null)
            blocos = listarBlocos();
        blocos.add(bk);
    }
    
    public void excluirBloco(String sigla) throws PermissaoException, PersistenceExeception, SalaInvalida{
        usrLogado.excluirBloco(sigla);
        initBlocos();
        blocos.removeIf((bk)->{return bk.getSigla().equals(sigla);});
    }
    
    public Collection<Bloco> listarBlocos() throws PermissaoException, PersistenceExeception, SalaInvalida{
        return usrLogado.listarBlocos();
    }
    
    public void atualizarBloco(Bloco bk) throws PermissaoException, PersistenceExeception{
        usrLogado.atualizarBloco(bk);
    }
    
    private void initBlocos() throws PermissaoException, PersistenceExeception, SalaInvalida{
        if(blocos == null){
            blocos = listarBlocos();
            if(blocos == null){
                blocos = new ArrayList<>();
            }
        }
    }

    /**
     * Retira o usuario logado do sistema.
     */
    public void logoff(){
        this.usrLogado = null;
        this.usuarios = null;
    }
 
       
    
    
    
}
