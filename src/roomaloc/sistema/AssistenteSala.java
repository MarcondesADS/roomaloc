package roomaloc.sistema;

import java.util.ArrayList;
import java.util.Collection;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.gerenciamento.GerenciadorBloco;
import roomaloc.sistema.gerenciamento.GerenciadorProfessor;

/**
 * Classe de ações de Assistente de Sala
 * @author José Marcondes do Nascimento Junior
 */
public class AssistenteSala extends Usuario{

	public	 AssistenteSala(String login, String senha, String matricula) {
		super(login, senha, matricula);
		
	}
        public	 AssistenteSala(String login, String senha, String matricula,String und){
            super(login, senha, matricula,und);
        }
        
        private void cadastrarUsuario(Professor prof) throws PersistenceExeception{
            GerenciadorProfessor gProf = new GerenciadorProfessor();
            gProf.guardar(prof);
        }
        
        @Override
        public void excluirUsuario(String login) throws PersistenceExeception{
            GerenciadorProfessor gProf = new GerenciadorProfessor();
            gProf.excluir(login);
        }
        /**
         * {@inheritDoc } 
         */
        @Override
        public void cadastrarUsuario(Usuario usr) throws PermissaoException,PersistenceExeception{
            try{
                cadastrarUsuario((Professor) usr);
            }catch(ClassCastException ex){
                throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
            }
        }
        
        /**
         * Lista todos os professores do sistema.
         * @return Collection de professores.
         * @throws PersistenceExeception Erro de acesso aos dados.
         */
        @Override
        public Collection<Usuario> listarUsuarios() throws PersistenceExeception{
            return new ArrayList<>(new GerenciadorProfessor().listar());
        }
        
        @Override
        public void cadastrarBloco(Bloco bk) throws PermissaoException, PersistenceExeception{
            new GerenciadorBloco().guardarBloco(bk);
        }
        
        @Override
       public void excluirBloco(String sigla) throws PermissaoException, PersistenceExeception{
            new GerenciadorBloco().excluirBloco(sigla);
        }
        
        @Override
        public void atualizarBloco(Bloco bk) throws PermissaoException, PersistenceExeception{
            new GerenciadorBloco().update(bk);
        }
        
        @Override
        public Collection<Bloco> listarBlocos() throws PermissaoException, PersistenceExeception, SalaInvalida{
            return new GerenciadorBloco().listar();
        }

}
