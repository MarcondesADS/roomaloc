/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.sistema;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class SalaInvalida extends Exception {

    public SalaInvalida() {
    }

    public SalaInvalida(String message) {
        super(message);
    }

    public SalaInvalida(String message, Throwable cause) {
        super(message, cause);
    }

    public SalaInvalida(Throwable cause) {
        super(cause);
    }

    public SalaInvalida(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    
    
}
