
package roomaloc.sistema;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.gerenciamento.GerenciadorSala;

/**
 * Classe de representação de um setor de áreas fisicas (Salas), a sigla para cada
 * bloco é automaticamente criada utilizando as letras maiusculas no nome do bloco criado.
 * @author José Marcondes do Nascimento Junior
 */
public class Bloco {
    private String nome;
    private String sigla;
    private Collection<Sala> salas;

    public Bloco(String nome) {
        this.nome = nome;
        salas = new ArrayList<>();
        sigla = createSigla(nome);
    }
    
    

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSigla() {
        return sigla;
    }

    public void setSigla(String sigla) {
        this.sigla = sigla;
    }

    /**
     * Retorna uma coleção imutável de salas.
     * @return salas.
     */
    public Collection<Sala> getSalas() {
        if(salas != null)
            return Collections.unmodifiableCollection(salas);
        else
            return null;
    }

    public void addSala(Sala sala) throws PersistenceExeception {
        salas.add(sala);
        new GerenciadorSala().guardarSala(sala, this);
    }

    public void setSalas(Collection<Sala> salas) {
        this.salas = salas;
    }
    
    private String createSigla(String nome){
        String[] s = nome.split("[a-z]");
        String sigla = "";
        for(String ss: s){
            sigla = sigla.concat(ss);
        }
        sigla = sigla.replaceAll("\\s", "");
        return sigla;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + Objects.hashCode(this.sigla);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bloco other = (Bloco) obj;
        if (!Objects.equals(this.sigla, other.sigla)) {
            return false;
        }
        return true;
    }

   
    

}
