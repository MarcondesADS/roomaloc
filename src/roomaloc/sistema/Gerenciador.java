package roomaloc.sistema;

import java.util.Collection;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * Interface para para usuários gerenciadores de sistema.
 * @author José Marcondes do Nascimento Junior. 
 *
 */
public interface Gerenciador {
	/**
	 * Cadastra um novo usuário, o login do usuário não deve ser repetido no sistema.
	 * @param usr Novo usuário à ser cadastrado.
         * @throws roomaloc.sistema.dao.PersistenceExeception  Erro de acesso a base de dados
	 * @throws PermissaoException O usuário não possui permição para executar esta tarefa.
	 */
	default public void cadastrarUsuario(Usuario usr) throws PersistenceExeception, PermissaoException{
		throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
	}
	
	/**
	 * Exclui um usuário do sistema
	 * @param login login do usuário a ser excluido.
	 * @throws PermissaoException O usuário não possui permição para executar esta tarefa.
         * @throws roomaloc.sistema.dao.PersistenceExeception Erro no acesso aos dados.
	 */
	default public void excluirUsuario(String login) throws PermissaoException, PersistenceExeception{
		throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
	}
	/**
         * Método para listar usuarios.
         * @return Coleção de usuarios;
         * @throws PermissaoException O usuario não tem permição suficiente para esta ação.
         * @throws PersistenceExeception Erro de acesso aos dados.
         */
        default public Collection<Usuario> listarUsuarios() throws PermissaoException, PersistenceExeception{
           throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
        }
        
        /**
         * Método para cadastrar um novo bloco.
         * @param bk Bloco a ser cadastrado
         * @throws PermissaoException O usuario não tem permição suficiente para esta ação.
         * @throws PersistenceExeception Erro de acesso aos dados.
         */
        default public void cadastrarBloco(Bloco bk) throws PermissaoException, PersistenceExeception{
            throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
        }
        
        /**
         * Exclui um bloco da base de dados
         * @param sigla Sigla do bloco a ser excluido.
         * @throws PermissaoException O usuario não tem permição suficiente para esta ação.
         * @throws PersistenceExeception Erro de acesso aos dados.
         */
        default public void excluirBloco(String sigla) throws PermissaoException, PersistenceExeception{
            throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
        }
        
        /**
         * Atualiza um bloco e suas salas na base de dados.
         * @param bk Bloco a ser atualizado
         * @throws PermissaoException O usuario não tem permição suficiente para esta ação.
         * @throws PersistenceExeception Erro de acesso aos dados.
         */
        default public void atualizarBloco(Bloco bk) throws PermissaoException, PersistenceExeception{
            throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
        }
        
        /**
         * Retorna uma coleção de objetos bloco presente no sistema.
         * @return Collection de blocos.
         * @throws PermissaoException O usuario não tem permição suficiente para esta ação.
         * @throws PersistenceExeception Erro de acesso aos dados.
         * @throws SalaInvalida Sala em estado inconsistente.
         */
        default public Collection<Bloco> listarBlocos() throws PermissaoException, PersistenceExeception, SalaInvalida{
            throw new PermissaoException("O usuário não possui permição para executar esta tarefa");
        }
	
}
