package roomaloc.sistema.dao;

import java.util.Collection;

import roomaloc.sistema.Usuario;

/**
 * Interface que define classes de acesso aos dados de objetos Usuario na base de dados.
 * @author José Marcondes do Nascimento Junior.
 *
 */
public interface UsuarioDAO {

	/**
	 * Guarda um objeto do tipo Usuario com o tipo especificado na base de dados
	 * @param usr Usuario que será guardado
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardar(Usuario usr) throws PersistenceExeception;
	
	/**
	 * Exclui um objeto do tipo Usuario com o tipo especificado da base de dados. 
	 * @param login Login do Usuario a ser excluído.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluir(String login) throws PersistenceExeception;
	
	/**
	 * Lista todos os objetos do tipo Usuario com o tipo especificado existentes na base de dados.
	 * @return Collection com todos os objetos Usuario da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public Collection<Usuario> listar() throws PersistenceExeception;
	
	/**
	 * Verifica se um determinado objeto do tipo Usuario com o tipo especificado existe na base de dados.
	 * @param login Login do Usuario à ser verificado.
	 * @return Retorna verdadeiro caso o Usuario exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean exist(String login) throws PersistenceExeception;
	
	/**
         * Atualiza dados de um Usuarios.
         * @param usr Usuario a ser atualizado (login) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         */
        public  void update(Usuario usr) throws PersistenceExeception;

}
