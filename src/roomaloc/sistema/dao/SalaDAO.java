
package roomaloc.sistema.dao;

import java.util.Collection;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.Sala;
import roomaloc.sistema.SalaInvalida;

/**
 * Interface de Dao para salas
 * @author José Marcondes do Nascimento Junior
 */
public interface SalaDAO {
        
        /**
	 * Guarda um objeto Sala na base de dados.
	 * @param sl Sala que será guardada
         * @param bk Bloco da sala que será guardada
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardar(Sala sl, Bloco bk) throws PersistenceExeception;
	
	/**
	 * Exclui um objeto Sala da base de dados. 
	 * @param id Id da Sala a ser excluída.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluir(String id) throws PersistenceExeception;
	
	/**
	 * Lista todos os objetos Sala existentes na base de dados.
	 * @return Collection com todos os objetos Sala da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
         * @throws SalaInvalida Sala recuperada num estado inválido
	 */
	public Collection<Sala> listar() throws PersistenceExeception, SalaInvalida;
	
	/**
	 * Verifica se um determinado objeto Sala existe na base de dados.
	 * @param id Id da Sala à ser verificada.
	 * @return Retorna verdadeiro caso a sala exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean exist(String id) throws PersistenceExeception;
	
	
	/**
	 * Localiza uma determinada Sala na base de dados.
	 * @param id Id da Sala que deve ser localizado.
	 * @return O objeto Sala caso esse seja encontrado ou null caso não exista esta sala.
	 * @throws PersistenceExeception Erro no acesso aos dados.
         * @throws SalaInvalida Sala recuperada num estado inválido
	 */
	public Sala localiza(String id) throws PersistenceExeception, SalaInvalida;
        
        /**
         * Atualiza dados de uma Sala, caso esta sala não exista a mesma é guardada no banco de dados
         * no bloco passado por parâmetro.
         * @param  bk Bloco caso a sala não exista.
         * @param sl Sala a ser atualizado (id) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         * 
         */
        public  void update(Sala sl, Bloco bk) throws PersistenceExeception;
}
