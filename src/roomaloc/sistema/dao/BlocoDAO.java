
package roomaloc.sistema.dao;

import java.util.Collection;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.SalaInvalida;

/**
 * Interface de DAO para blocos
 * @author José Marcondes do Nascimento Junior
 */
public interface BlocoDAO {
    
        /**
	 * Guarda um objeto Bloco na base de dados.
	 * @param bk Sala que será guardada
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardar(Bloco bk) throws PersistenceExeception;
	
	/**
	 * Exclui um objeto Bloco da base de dados. 
	 * @param sigla Sigla do Bloco a ser excluído.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluir(String sigla) throws PersistenceExeception;
	
	/**
	 * Lista todos os objetos Bloco existentes na base de dados.
	 * @return Collection com todos os objetos Bloco da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
         * @throws SalaInvalida Uma das salas deste bloco possui um estado inválido.
	 */
	public Collection<Bloco> listar() throws PersistenceExeception, SalaInvalida;
	
	/**
	 * Verifica se um determinado objeto Bloco existe na base de dados.
	 * @param sigla Sigla do Bloco à ser verificada.
	 * @return Retorna verdadeiro caso o bloco exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean exist(String sigla) throws PersistenceExeception;
	
	
	/**
	 * Localiza um determinado Bloco na base de dados.
	 * @param sigla Sigla do Bloco que deve ser localizado.
	 * @return O objeto Bloco caso esse seja encontrado ou null caso não existe bloco.
	 * @throws PersistenceExeception Erro no acesso aos dados.
         * @throws SalaInvalida Uma das salas deste bloco possui um estado inválido.
	 */
	public Bloco localiza(String sigla) throws PersistenceExeception, SalaInvalida;
        
        /**
         * Atualiza dados de um Bloco.
         * @param bk Bloco a ser atualizado (sigla) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         */
        public  void update(Bloco bk) throws PersistenceExeception;
}
