package roomaloc.sistema.dao;

/**
 * Interface que define classes de fabrica de DAO's
 * @author José Marcondes do Nascimento Junior
 *
 */
public interface DAOFactory {

	/**
	 * Cria um DAO de Administrador
	 * @return DAO Administrador
	 */
	public AdministradorDAO createAdmDAO();
	
	/**
	 * Cria um DAO de AssistenteSala
	 * @return DAO AssistenteSala
	 */
	public AssistenteSalaDAO createAsSDAO();
	
	/**
	 * Cria um DAO de Professor
	 * @return DAO Professor
	 */
	public ProfessorDAO createProfDAO();
        
        /**
         * Cria um DAO para Sala.
         * @return DAO de Sala.
         */
        public SalaDAO createSalaDAO();
        
        /**
         * Cria um DAO para Bloco.
         * @return DAO de Bloco.
         */
        public BlocoDAO createBlocoDAO();
	
}
