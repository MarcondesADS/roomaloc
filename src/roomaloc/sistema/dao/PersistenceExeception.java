package roomaloc.sistema.dao;

public class PersistenceExeception extends Exception {

	public PersistenceExeception() {
		super();
	}
	
	public PersistenceExeception(String msg) {
		super(msg);
	}
	
}
