package roomaloc.sistema.dao;

import java.util.Collection;

import roomaloc.sistema.AssistenteSala;
/**
 * Interface que define um classe para acesso aos dados de objetos AssitenteSala na base de dados.
 * @author José Marcondes do Nascimento Junior.
 *
 */

public interface AssistenteSalaDAO {
	
	/**
	 * Guarda um objeto do tipo AssistenteSala na base de dados
	 * @param ass AssistenteSala que será guardado
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardar(AssistenteSala ass) throws PersistenceExeception;
	
	/**
	 * Exclui um objeto do tipo AssistenteSala da base de dados. 
	 * @param login Login do AssistenteSala a ser excluído.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluir(String login) throws PersistenceExeception;
	
	/**
	 * Lista todos os objetos do tipo AssistenteSala existentes na base de dados.
	 * @return Collection com todos os AssistenteSala da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public Collection<AssistenteSala> listar() throws PersistenceExeception;
	
	/**
	 * Verifica se um determinado objeto do tipo AssistenteSala existe na base de dados.
	 * @param login Login do AssistenteSala à ser verificado.
	 * @return Retorna verdadeiro caso o Professor exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean exist(String login) throws PersistenceExeception;
	
	
	/**
	 * Localiza um determinado AssistenteSala na base de dados.
	 * @param login Login do AssistenteSala que deve ser localizado.
	 * @return O objeto AssistenteSala caso esse seja encontrado ou null caso não exista este AssistenteSala.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public AssistenteSala localiza(String login) throws PersistenceExeception;
        
        /**
         * Atualiza dados de um AssistenteSala
         * @param usr AssistenteSala a ser atualizado (login) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         */
        public  void update(AssistenteSala usr) throws PersistenceExeception;
}
