package roomaloc.sistema.dao;

import java.util.Collection;
import roomaloc.sistema.Professor;

/**
 * Interface que define classes de acesso aos dados de objetos Professor na base de dados.
 * @author José Marcondes do Nascimento Junior.
 *
 */
public interface ProfessorDAO {

	/**
	 * Guarda um objeto do tipo Professor na base de dados
	 * @param prof Professor que será guardado
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardar(Professor prof) throws PersistenceExeception;
	
	/**
	 * Exclui um objeto do tipo Professor da base de dados. 
	 * @param login Login do Professor a ser excluído.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluir(String login) throws PersistenceExeception;
	
	/**
	 * Lista todos os objeto do tipo Professor existentes na base de dados.
	 * @return Collection com todos os objetos Professor da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public Collection<Professor> listar() throws PersistenceExeception;
	
	/**
	 * Verifica se um determinado objeto do tipo Professor existe na base de dados.
	 * @param login Login do Professor à ser verificado.
	 * @return Retorna verdadeiro caso o Professor exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean exist(String login) throws PersistenceExeception;
	
	
	
	/**
	 * Localiza um determinado Professor na base de dados.
	 * @param login Login do Professor que deve ser localizado.
	 * @return O objeto Professor caso esse seja encontrado ou null caso não exista este Professor.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public Professor localiza(String login) throws PersistenceExeception;
        
        /**
         * Atualiza dados de um AssistenteSala
         * @param usr AssistenteSala a ser atualizado (login) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         */
        public  void update(Professor usr) throws PersistenceExeception;
}
