package roomaloc.sistema.dao;

import java.sql.SQLException;

import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.daobd.*;
import roomaloc.sistema.dao.daobd.DAOFactoryBD;
/**
 * Classe concreta de fabrica de DAOFactory's
 * @author José Marcondes do Nascimento Junior.
 *
 */
public abstract class PersistenceFactory{

	public static final int BANCO_DE_DADOS = 1;
	
	/**
	 * Cria uma DAOFactory de acordo com o tipo especificado no parâmetro.
	 * @param tipo Tipo de factory que deverá ser criada. (tipos contidos na interface PersistenceFactory).
	 * @return A DAOFactory do tipo passado.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public static DAOFactory criarDAOFactory(int tipo) throws PersistenceExeception{
		switch(tipo){
			case BANCO_DE_DADOS:{
				try{
					ConnectionManager conn = new ConnectionManager();
					DAOFactory daof = new DAOFactoryBD(conn.pegarConnection());
					return daof;
				}catch (SQLException ex) {
					throw new PersistenceExeception(ex.getMessage());
				}
			}
		}
		return null;
	}

}
