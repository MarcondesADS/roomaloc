package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

import roomaloc.sistema.Administrador;
import roomaloc.sistema.AssistenteSala;
import roomaloc.sistema.Professor;
import roomaloc.sistema.Usuario;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.UsuarioDAO;

/**
 * Classe de acesso aos dados de objetos to tipo Usuário com seu determinado tipo no banco de dados.
 * @author José Marcondes do Nascimento Junior.
 *
 */
class UsuarioDAOBD implements UsuarioDAO {
	private Connection conn;
	private int tipo;
	
	/**
	 * Cria um objeto do tipo UsuarioDAOBD que utilizará uma conexão e o o tipo de usuário passado
	 * para realizar os métodos.
	 * @param conn Conexão com o Banco de dados.
	 * @param tipo Tipo de usuário que está sendo tratado.
	 */
	public UsuarioDAOBD(Connection conn, int tipo) {
		this.conn = conn;
		this.tipo = tipo;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void guardar(Usuario usr) throws PersistenceExeception{
	
		String login = usr.getLogin();
		String senha = usr.getSenha();
		String matricula = usr.getMatricula();
		String undAcademica = usr.getUndAcademica();
		PreparedStatement ps = null;
		try{
		    ps = conn.prepareStatement("INSERT INTO USUARIO VALUES (?,?,?,?,?)");
		    ps.setString(1, login);
		    ps.setString(2, senha);
		    ps.setString(3, matricula);
		    ps.setString(4, undAcademica);
		    ps.setInt(5, tipo);
		    ps.executeUpdate();
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
	    	try{ps.close();}catch(Exception ex){
	    		
	    	}
	    }

	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void excluir(String login) throws PersistenceExeception {
		PreparedStatement ps = null;
		try{
			ps = conn.prepareStatement("DELETE FROM USUARIO WHERE login = ? AND tipo = ?");
			ps.setString(1, login);
			ps.setInt(2, tipo);
			if(ps.executeUpdate() == 0)
				throw new SQLException("Usuário inexistente.");
		
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
			try{ps.close();}catch(Exception ex){}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Usuario> listar() throws PersistenceExeception{
		Statement stm = null;
		ResultSet rs = null;
		try{
			stm = conn.createStatement();
			Collection<Usuario> list = Collections.emptyList();
			rs = stm.executeQuery("SELECT * FROM USUARIO WHERE tipo = "+tipo);
			
			while(rs.next()){
				String login = rs.getString("login");
				String senha = rs.getString("senha");
				String matricula = rs.getString("matricula");
				String undAcademica = rs.getString("und_Academica");
                                Usuario usr = null;
                                switch(tipo){
                                    case 1:usr = new Administrador(login, senha, matricula);
                                        break;
                                    case 2: usr = new AssistenteSala(login, senha, matricula);
                                        break;
                                    case 3: usr = new Professor(login, senha, matricula);
                                   
                                }
				usr.setUndAcademica(undAcademica);
				
				if(list.equals(Collections.EMPTY_LIST)){
					list = new ArrayList<Usuario>();
				}
				
				list.add(usr);
				
			}
			
			return list;
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
			
			try{rs.close();}catch(Exception ex){}
			try{stm.close();}catch(Exception ex){}
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean exist(String login) throws PersistenceExeception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(
					"SELECT login "
					+ "FROM USUARIO "
					+ "WHERE login = ? AND tipo = ?");
			ps.setString(1, login);
			rs = ps.executeQuery();
			if(rs.next())
                            return true;
			return false;						
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
			try{rs.close();}catch(Exception ex){}
			try{ps.close();}catch(Exception ex){}
		}
	}


    @Override
    public void update(Usuario usr) throws PersistenceExeception{
       String login = usr.getLogin();
		String senha = usr.getSenha();
		String matricula = usr.getMatricula();
		String undAcademica = usr.getUndAcademica();
		PreparedStatement ps = null;
		try{
		    ps = conn.prepareStatement("UPDATE USUARIO SET senha = ?, matricula =? , und_Academica = ? "
                            + "WHERE login = ?");
		    ps.setString(1, senha);
		    ps.setString(2, matricula);
		    ps.setString(3, undAcademica);
                    ps.setString(4, login);
		    ps.executeUpdate();
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
                    try{ps.close();}catch(Exception ex){}
                }

    }
    
    
}