
package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.Sala;
import roomaloc.sistema.SalaBuilder;
import roomaloc.sistema.SalaInvalida;
import roomaloc.sistema.TipoSala;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.SalaDAO;

/**
 * Classe de DAO de salas em um banco de dados relacionais.
 * @author José Marcondes do Nascimento Junior
 */
public class SalaDAOBD implements SalaDAO{
    private Connection conn;
    private final int NORMAL = 1;
    private final int INTELIGENTE = 2;
    private final int LABORATORIO = 3;

    public SalaDAOBD(Connection conn) {
        this.conn = conn;
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void guardar(Sala sl, Bloco bk) throws PersistenceExeception {
        try(PreparedStatement ps = conn.prepareStatement("INSERT INTO SALA VALUES (?,?,?,?,?,?)");){
            ps.setString(1, sl.getId());
            ps.setString(2,sl.getNome());
            ps.setInt(3, sl.getCapacidade());
            ps.setString(4, sl.getApelido());
            int tipo = sl.getTipo() == TipoSala.NORMAL?NORMAL:
                    (sl.getTipo() == TipoSala.INTELIGENTE?INTELIGENTE:LABORATORIO);
            ps.setInt(5, tipo);
            ps.setString(6, bk.getSigla());
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void excluir(String id) throws PersistenceExeception {
        try(PreparedStatement ps = conn.prepareStatement("DELETE FROM SALA WHERE id = ?");){
            ps.setString(1, id);
            ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public Collection<Sala> listar() throws PersistenceExeception, SalaInvalida {
        Collection<Sala> salas = null;
        try(Statement stm = conn.createStatement();){
            try(ResultSet rs = stm.executeQuery("SELECT * FROM SALA");){
                while(rs.next()){
                    if(salas == null)
                        salas = new ArrayList<>();
                    String id = rs.getString("id");
                    String nome = rs.getString("nome");
                    int capacidade = rs.getInt("capacidade");
                    String apelido = rs.getString("apelido");
                    int tipo = rs.getInt("tipo");
                    TipoSala ts = tipo == NORMAL?TipoSala.NORMAL:
                            (tipo == INTELIGENTE?TipoSala.INTELIGENTE: TipoSala.LABORATORIO);
                    Sala sl = new SalaBuilder().comId(id).comNome(nome).comCapacidadeDe(capacidade)
                            .comApelido(apelido).doTipo(ts).build();
                    salas.add(sl);
                }
                return salas;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public boolean exist(String id) throws PersistenceExeception {
        Collection<Sala> salas = null;
        try(Statement stm = conn.createStatement();){
            try(ResultSet rs = stm.executeQuery("SELECT * FROM SALA");){
                if(rs.next())
                    return true;
                return false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public Sala localiza(String id) throws PersistenceExeception, SalaInvalida{
        try(PreparedStatement ps = conn.prepareStatement("SELECT * FROM SALA WHERE id = ?")){
            try(ResultSet rs = ps.executeQuery();){
                if(rs.next()){
                    String nome = rs.getString("nome");
                    int capacidade = rs.getInt("capacidade");
                    String apelido = rs.getString("apelido");
                    int tipo = rs.getInt("tipo");
                    TipoSala ts = tipo == NORMAL?TipoSala.NORMAL:
                            (tipo == INTELIGENTE?TipoSala.INTELIGENTE: TipoSala.LABORATORIO);
                    Sala sl = new SalaBuilder().comId(id).comNome(nome).comCapacidadeDe(capacidade)
                            .comApelido(apelido).doTipo(ts).build();
                    return sl;
                }
                    
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void update(Sala sl, Bloco bk) throws PersistenceExeception {
        try(PreparedStatement ps = conn.prepareStatement("UPDATE SALA SET capacidade = ?, apelido = ?, "
                + "tipo = ? WHERE id = ?");){
            ps.setInt(1, sl.getCapacidade());
            ps.setString(2, sl.getApelido());
            int tipo = sl.getTipo() == TipoSala.NORMAL?NORMAL:
                    (sl.getTipo() == TipoSala.INTELIGENTE?INTELIGENTE:LABORATORIO);
            ps.setInt(3, tipo);
            ps.setString(4, sl.getId());
            if(ps.executeUpdate() < 1){
                guardar(sl, bk);
            }
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }
    
    /**
     * Retorna as salas de um determinado bloco.
     * @param sigla sigla do bloco que possui as salas
     * @return Coleção com as salas recuperadas.
     */
    public Collection<Sala> listar(String sigla) throws PersistenceExeception, SalaInvalida{
        Collection<Sala> salas = null;
        try(PreparedStatement ps = conn.prepareStatement("SELECT * FROM SALA WHERE siglaBk = ?")){
           ps.setString(1, sigla);
           try(ResultSet rs = ps.executeQuery()){
               while(rs.next()){
                    if(salas == null)
                        salas = new ArrayList<>();
                    String nome = rs.getString("nome");
                    String id = rs.getString("id");
                    int capacidade = rs.getInt("capacidade");
                    String apelido = rs.getString("apelido");
                    int tipo = rs.getInt("tipo");
                    TipoSala ts = tipo == NORMAL?TipoSala.NORMAL:
                            (tipo == INTELIGENTE?TipoSala.INTELIGENTE: TipoSala.LABORATORIO);
                    Sala sl = new SalaBuilder().comId(id).comNome(nome).comCapacidadeDe(capacidade)
                            .comApelido(apelido).doTipo(ts).build();
                    salas.add(sl);
                }
               return salas;
           }
        } catch (SQLException ex) {
            Logger.getLogger(SalaDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }
    
}
