package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import roomaloc.sistema.Professor;
import roomaloc.sistema.Usuario;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.ProfessorDAO;
import roomaloc.sistema.dao.UsuarioDAO;

/**
 * Classe de acesso aos dados de professor no banco de dados.
 * @author José Marcondes do Nascimento Junior.
 */
public class ProfessorDAOBD implements ProfessorDAO{
	
	private final int TIPO_PROFESSOR = 3;
	private final UsuarioDAO daoUsuario;
	private final Connection conn;
	
	/**
	 * Cria um novo objeto ProfessorDAOBD que utilizará uma conexão com o banco de dados para
	 * realizar os métodos.
	 * @param conn Conexão com o banco de dados.
	 */
	public ProfessorDAOBD(Connection conn) {
		daoUsuario = new UsuarioDAOBD(conn, TIPO_PROFESSOR);
		this.conn = conn;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void guardar(Professor prof) throws PersistenceExeception {
		daoUsuario.guardar(prof);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void excluir(String login) throws PersistenceExeception {
		daoUsuario.excluir(login);		
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Professor> listar() throws PersistenceExeception {
		Collection<Professor> list = null;
		for (Usuario usr : daoUsuario.listar()) {
			if(list == null){
				list = new ArrayList<>();
			}
			Professor prof = (Professor) usr;
			list.add(prof);
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean exist(String login) throws PersistenceExeception {
		return daoUsuario.exist(login);
	}


	@Override
	public Professor localiza(String login) throws PersistenceExeception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(
					"SELECT * "
					+ "FROM USUARIO "
					+ "WHERE login = ? AND tipo = 3");
			ps.setString(1, login);
			rs = ps.executeQuery();
			if(rs.next()){
				String senha = rs.getString("senha");
				String matricula = rs.getString("matricula");
				String undAcademica = rs.getString("und_Academica");
				Professor prof = new Professor(login, senha, matricula);
				prof.setUndAcademica(undAcademica);
				return prof;
			}
			return null;
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
			try{rs.close();}catch(Exception ex){throw new PersistenceExeception(ex.getMessage());}
			try{ps.close();}catch(Exception ex){throw new PersistenceExeception(ex.getMessage());}
		}
	}

    @Override
    public void update(Professor usr) throws PersistenceExeception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
