package roomaloc.sistema.dao.daobd;

import roomaloc.sistema.dao.DAOFactory;
import java.sql.Connection;
import roomaloc.sistema.dao.*;

/**
 * Classe criadora de Objetos de acesso aos dados em banco de dados.
 * @author José Marcondes do Nascimento Junior
 *
 */
public class DAOFactoryBD implements DAOFactory {

    private Connection conn;

    public DAOFactoryBD(Connection conn) {
            this.conn = conn;
    }

    @Override
    public AdministradorDAO createAdmDAO() {
            return new AdministradorDAOBD(conn);
    }

    @Override
    public AssistenteSalaDAO createAsSDAO() {
            return new AssistenteSalaDAOBD(conn);
    }

    @Override
    public ProfessorDAO createProfDAO() {
            return new ProfessorDAOBD(conn);
    }

    @Override
    public SalaDAO createSalaDAO() {
        return new SalaDAOBD(conn);
    }

    @Override
    public BlocoDAO createBlocoDAO() {
        return new BlocoDAOBD(conn);
    }

}
