
package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.Sala;
import roomaloc.sistema.SalaInvalida;
import roomaloc.sistema.dao.BlocoDAO;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * Classe de acesso aos dados de bloco em banco de dados relacionais.
 * @author José Marcondes do Nascimento Junior
 */
public class BlocoDAOBD implements BlocoDAO{

    private final Connection conn;

    public BlocoDAOBD(Connection conn) {
        this.conn = conn;
    }
    
    /**
     * {@inheritDoc} 
     */
    @Override
    public void guardar(Bloco bk) throws PersistenceExeception {
        try{
            conn.setAutoCommit(false);
            try(PreparedStatement ps = 
                    conn.prepareStatement("INSERT INTO BLOCO VALUES (?,?)");){
                ps.setString(1, bk.getSigla());
                ps.setString(2, bk.getNome());
                ps.executeUpdate();
                SalaDAOBD daoSl = new SalaDAOBD(conn);
                for(Sala sl: bk.getSalas()){
                    daoSl.guardar(sl, bk);
                }
                conn.commit();
                conn.setAutoCommit(true);
            }
        }catch(SQLException ex){
            try{
                conn.rollback();
                conn.setAutoCommit(true);
                throw new PersistenceExeception(ex.getMessage());
            }catch(SQLException ex2){
                throw new PersistenceExeception(ex2.getMessage());
            }        
        }        
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void excluir(String sigla) throws PersistenceExeception {
        try(PreparedStatement ps = 
                conn.prepareStatement("DELETE FROM BLOCO WHERE sigla = ?");){
            ps.setString(1, sigla);
            ps.executeUpdate();
        }catch(SQLException ex){
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public Collection<Bloco> listar() throws PersistenceExeception, SalaInvalida{
        Collection<Bloco> bks = null;
        try(PreparedStatement ps = 
                conn.prepareStatement("SELECT * FROM BLOCO");){
            try(ResultSet rs = ps.executeQuery()){
                while(rs.next()){
                    if(bks == null){
                        bks = new ArrayList<>();
                    }
                    Bloco bk = new Bloco(rs.getString("nome"));
                    bk.setSalas(new SalaDAOBD(conn).listar(bk.getSigla()));
                    bks.add(bk);
                }
                return bks;
            }
        }catch (SQLException ex) {
            Logger.getLogger(BlocoDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public boolean exist(String sigla) throws PersistenceExeception {
        try(PreparedStatement ps = 
                conn.prepareStatement("SELECT COUNT(*) count FROM BLOCO");){
            try(ResultSet rs = ps.executeQuery()){
                if(rs.next()){
                    if(rs.getInt("cont") >= 1)
                        return true;
                    return false;
                }
            }
            return false;
        }catch (SQLException ex) {
            Logger.getLogger(BlocoDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public Bloco localiza(String sigla) throws PersistenceExeception, SalaInvalida{
        try(PreparedStatement ps = 
                conn.prepareStatement("SELECT * FROM BLOCO WHERE sigla = ?");){
            ps.setString(1, sigla);
            try(ResultSet rs = ps.executeQuery()){
                if(rs.next()){
                    Bloco bk = new Bloco(rs.getString("nome"));
                    bk.setSalas(new SalaDAOBD(conn).listar(sigla));
                    return bk;
                }
                return null;
            }
        }catch (SQLException ex) {
            Logger.getLogger(BlocoDAOBD.class.getName()).log(Level.SEVERE, null, ex);
            throw new PersistenceExeception(ex.getMessage());
        }
    }

    /**
     * {@inheritDoc} 
     */
    @Override
    public void update(Bloco bk) throws PersistenceExeception {
        try{
            conn.setAutoCommit(false);
            try(PreparedStatement ps = 
                    conn.prepareStatement("UPDATE BLOCO SET nome = ? WHERE sigla = ?");){
                ps.setString(1, bk.getNome());
                ps.setString(2, bk.getSigla());
                ps.executeUpdate();
                SalaDAOBD daoSl = new SalaDAOBD(conn);
                if(bk.getSalas() != null )
                    for(Sala sl: bk.getSalas()){
                        daoSl.update(sl,bk);
                    }
                conn.commit();
                conn.setAutoCommit(true);
            }
        }catch(SQLException ex){
            try{
                conn.rollback();
                conn.setAutoCommit(true);
                throw new PersistenceExeception(ex.getMessage());
            }catch(SQLException ex2){
                throw new PersistenceExeception(ex2.getMessage());
            }        
        } 
    }
    
        
}
