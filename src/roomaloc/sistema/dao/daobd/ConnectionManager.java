package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import roomaloc.config.Configuracao;


/**
 * Classe de gerenciamento de conexão com banco de dados.
 * @author José Marcondes do Nascimento Junior
 *
 */
public class ConnectionManager{

	private static String url;
	private static String nome;
	private static String senha;
	private static Connection conn;
	
	/**
         * Pega uma conexão unica no banco de dados, a mesma conexão será mandada para todas as chamadas deste método.
         * @return Conexão com o banco de dados.
         * @throws SQLException Erro no acesso ao banco de dados.
         */
	public static Connection pegarConnection() throws SQLException{
            if(conn == null){
                Configuracao.initConnectionConfig();
                conn =  DriverManager.getConnection(url,nome,senha);
            }
                return conn;
	}

    public static String getUrl() {
        return url;
    }

    public static void setUrl(String url) {
        ConnectionManager.url = url;
    }

    public static String getNome() {
        return nome;
    }

    public static void setNome(String nome) {
        ConnectionManager.nome = nome;
    }

    public static String getSenha() {
        return senha;
    }

    public static void setSenha(String senha) {
        ConnectionManager.senha = senha;
    }
	
}
