package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

import roomaloc.sistema.Administrador;
import roomaloc.sistema.Usuario;
import roomaloc.sistema.dao.AdministradorDAO;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.UsuarioDAO;

/**
 * Classe que realiza manipulação de objetos Administrador no banco de dados do sistema.
 * @author José Marcondes do Nascimento Junior. 
 */
public class AdministradorDAOBD implements AdministradorDAO {

	private final int TIPO_ADMINISTRADOR = 1;
	private final UsuarioDAO DAOUsuario;
	private Connection conn;
	
	/**
	 * Cria um novo objeto AdministradorDAOBD que utilizará uma conexão com um banco de dados para realizar
	 * as operações de cada método.
	 * @param conn Conexão com o banco de dados.
	 */
	public AdministradorDAOBD(Connection conn) {
		this.DAOUsuario = new UsuarioDAOBD(conn, TIPO_ADMINISTRADOR);
		this.conn = conn;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void guardar(Administrador adm) throws PersistenceExeception {
		DAOUsuario.guardar(adm);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void excluir(String login) throws PersistenceExeception {
		DAOUsuario.excluir(login);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<Administrador> listar() throws PersistenceExeception{
		Collection<Administrador> list = null;
		for (Usuario usr : DAOUsuario.listar()) {
			if(list == null){
				list = new ArrayList<>();
			}
			Administrador adm = (Administrador) usr;
			list.add(adm);
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean exist(String login) throws PersistenceExeception {
		return DAOUsuario.exist(login);
	}


	@Override
	public Administrador localiza(String login) throws PersistenceExeception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement("SELECT * FROM USUARIO WHERE login = ? AND tipo = 1");
			ps.setString(1, login);
			rs = ps.executeQuery();
			if(rs.next()){
				String senha = rs.getString("senha");
				String matricula = rs.getString("matricula");
				String undAcademica = rs.getString("und_Academica");
				Administrador adm = new Administrador(login, senha, matricula);
				adm.setUndAcademica(undAcademica);
				return adm;
			}
			return null;
		}catch(SQLException ex){
			System.err.print(ex.getMessage());
			Logger.getLogger(this.getClass().getName()).log(Level.SEVERE,null,ex);
			throw new PersistenceExeception(ex.getMessage());
		}finally{
			try{rs.close();}catch(Exception ex){Logger.getLogger(this.getClass().getName()).log(Level.WARNING,null,ex);}
			try{ps.close();}catch(Exception ex){Logger.getLogger(this.getClass().getName()).log(Level.WARNING,null,ex);}
		}
	}

    /**
     * {@inheritDoc} 
     */
    @Override
    public void update(Administrador usr) throws PersistenceExeception {
        DAOUsuario.update(usr);
    }

}
