package roomaloc.sistema.dao.daobd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import roomaloc.sistema.AssistenteSala;
import roomaloc.sistema.Usuario;
import roomaloc.sistema.dao.AssistenteSalaDAO;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.UsuarioDAO;

/**
 * Classe de acesso aos dados de objetos AssistenteSala num banco de dados.
 * @author José Marcondes do Nascimento Junior
 */
public class AssistenteSalaDAOBD implements AssistenteSalaDAO {

	private final int TIPO_ASSISTENTE_SALA = 2;
	private final UsuarioDAO daoUsuario;
	private final Connection conn;
	
	/**
	 * Cria um novo objeto do tipo AssistenteSalaDAOBD que utilizará uma conexão com o banco de dados
	 * para a realização do métodos.
	 * @param conn Conexão com o banco de dados.
	 */
	public AssistenteSalaDAOBD(Connection conn) {
		this.daoUsuario = new UsuarioDAOBD(conn, TIPO_ASSISTENTE_SALA);
		this.conn = conn;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void guardar(AssistenteSala ass) throws PersistenceExeception {
		daoUsuario.guardar(ass);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void excluir(String login) throws PersistenceExeception {
		daoUsuario.excluir(login);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Collection<AssistenteSala> listar() throws PersistenceExeception {
		Collection<AssistenteSala> list = null;
		for (Usuario usr : daoUsuario.listar()) {
			if(list == null){
				list = new ArrayList<>();
			}
			AssistenteSala ass = (AssistenteSala) usr;
			list.add(ass);
		}
		return list;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean exist(String login) throws PersistenceExeception {
		return daoUsuario.exist(login);
	}


	@Override
	public AssistenteSala localiza(String login) throws PersistenceExeception {
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			ps = conn.prepareStatement(
					"SELECT * "
					+ "FROM USUARIO "
					+ "WHERE login = ? AND tipo = 2");
			ps.setString(1, login);
			rs = ps.executeQuery();
			if(rs.next()){
				String senha = rs.getString("senha");
				String matricula = rs.getString("matricula");
				String undAcademica = rs.getString("und_Academica");
				AssistenteSala ass = new AssistenteSala(login, senha, matricula);
				ass.setUndAcademica(undAcademica);
				return ass;
			}
			return null;
		}catch(SQLException ex){
			throw new PersistenceExeception(ex.getMessage());
		}finally{
			try{rs.close();}catch(Exception ex){throw new PersistenceExeception(ex.getMessage());}
			try{ps.close();}catch(Exception ex){throw new PersistenceExeception(ex.getMessage());}
		}
	}

    @Override
    public void update(AssistenteSala usr) throws PersistenceExeception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
	

}
