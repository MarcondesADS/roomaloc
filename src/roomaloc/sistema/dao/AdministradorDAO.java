package roomaloc.sistema.dao;

import java.util.Collection;

import roomaloc.sistema.Administrador;

/**
 * Interface que representa um classe de acesso aos dados de objetos do tipo Administrador.
 * @author José Marcondes do Nascimento Junior.
 *
 */
public interface AdministradorDAO {

	/**
	 * Guarda um objeto Administrador na base de dados.
	 * @param adm Administrador que será guardado
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardar(Administrador adm) throws PersistenceExeception;
	
	/**
	 * Exclui um objeto Administrador da base de dados. 
	 * @param login Login do Administrador a ser excluído.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluir(String login) throws PersistenceExeception;
	
	/**
	 * Lista todos os objetos Administrador existentes na base de dados.
	 * @return Collection com todos os objetos Administrador da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public Collection<Administrador> listar() throws PersistenceExeception;
	
	/**
	 * Verifica se um determinado objeto Administrador existe na base de dados.
	 * @param login Login do Administrador à ser verificado.
	 * @return Retorna verdadeiro caso o administrador exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean exist(String login) throws PersistenceExeception;
	
	
	/**
	 * Localiza um determinado Administrador na base de dados.
	 * @param login Login do administrador que deve ser localizado.
	 * @return O objeto Administrador caso esse seja encontrado ou null caso não exista este administrador.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public Administrador localiza(String login) throws PersistenceExeception;
        
        /**
         * Atualiza dados de um Administrador
         * @param usr Administrador a ser atualizado (login) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         */
        public  void update(Administrador usr) throws PersistenceExeception;
}
