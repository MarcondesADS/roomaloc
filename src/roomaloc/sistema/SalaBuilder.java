
package roomaloc.sistema;

/**
 * Construtor de objetos do tipo Sala com válidação
 * @author José Marcondes do Nascimento Junior
 */
public class SalaBuilder {
    private String id;
    private String nome;
    private int capacidade;
    private String apelido;
    private TipoSala tipo;
    
    public SalaBuilder comId(String id){
        this.id = id;
        return this;
    }
    
    /**
     * Adiciona uma capacidade à uma sala, esta capacidade deve ser de no minimo 1.
     * @param cap Capacidade
     * @return Este objeto SalaBuilder
     * @throws IllegalArgumentException Inserido capacidade menor que 1;
     */
    public SalaBuilder comCapacidadeDe(int cap){
        if(cap < 1){
            throw new IllegalArgumentException("A sala deve possuir capacidade de no minimo 1");
        }
        this.capacidade = cap;
        return this;
    }
    
    public SalaBuilder comApelido(String apelido){
        this.apelido = apelido;
        return this;
    }
    public SalaBuilder doTipo(TipoSala ts){
        this.tipo = ts;
        return this;
    }
    
    public SalaBuilder comNome(String nome){
        this.nome = nome;
        return this;
    }
    
    public Sala build() throws SalaInvalida{
        if(this.isValid())
            return new Sala(id,nome, capacidade, apelido, tipo);
        else
            throw new SalaInvalida("A sala não pode ser construida no estado atual");
    }
    
    public boolean isValid(){
        return capacidade > 1 && id != null && tipo != null;
    }
    
}
