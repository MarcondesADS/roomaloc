package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.Sala;
import roomaloc.sistema.SalaInvalida;
import roomaloc.sistema.dao.DAOFactory;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.PersistenceFactory;
import roomaloc.sistema.dao.SalaDAO;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorSala {
    
    private final int tipo = PersistenceFactory.BANCO_DE_DADOS;
	
	/**
	 * Salva uma nova sala na base de dados.
	 * @param sl Nova Sala
         * @param bk Bloco que pussuirá esta sala na base de dados.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public void guardarSala(Sala sl, Bloco bk) throws PersistenceExeception{
		SalaDAO dao;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createSalaDAO();
		dao.guardar(sl,bk);
	}
	
	/**
	 * Exclui um objeto Sala da base de dados. 
	 * @param id Id da Sala a ser excluída.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluirSala(String id) throws PersistenceExeception{
		SalaDAO dao = null;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createSalaDAO();
		dao.excluir(id);
	}
	
	/**
	 * Verifica se um determinado objeto Sala existe na base de dados.
	 * @param sl Sala à ser verificada.
	 * @return Retorna verdadeiro caso a sala exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean contain(Sala sl) throws PersistenceExeception{
		SalaDAO dao;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createSalaDAO();
		return dao.exist(sl.getId());
		
	}
	
	/**
	 * Localiza uma determinada Sala na base de dados.
	 * @param id Id da Sala que deve ser localizado.
	 * @return O objeto Sala caso esse seja encontrado ou null caso não exista esta sala.
	 * @throws PersistenceExeception Erro no acesso aos dados.
         * @throws SalaInvalida Sala recuperada num estado inválido
	 */
	public Sala localiza(String id) throws PersistenceExeception, SalaInvalida{
            SalaDAO dao;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createSalaDAO();
            return dao.localiza(id);
	}
        
        /**
	 * Lista todos os objetos Sala existentes na base de dados.
	 * @return Collection com todos os objetos Sala da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
         * @throws SalaInvalida Sala recuperada num estado inválido
	 */
        public Collection<Sala> listar() throws PersistenceExeception, SalaInvalida{
            SalaDAO dao;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createSalaDAO();
            return dao.listar();
        }
        
        /**
         * Atualiza dados de uma Sala, caso esta sala não exista, esta é guardada na base de dados.
         * @param sl Sala a ser atualizado (id) e seus dados.
         * @param bk Bloco para guardar a sala caso não esteja guardada.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         * 
         */
        public void update(Sala sl, Bloco bk) throws PersistenceExeception{
            SalaDAO dao;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createSalaDAO();
            dao.update(sl, bk);
        }
    
}
