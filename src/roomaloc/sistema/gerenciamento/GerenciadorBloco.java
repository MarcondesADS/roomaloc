
package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.SalaInvalida;
import roomaloc.sistema.dao.BlocoDAO;
import roomaloc.sistema.dao.DAOFactory;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.PersistenceFactory;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorBloco {
    
    private final int tipo = PersistenceFactory.BANCO_DE_DADOS;
	
	/**
	 * Guarda um objeto Bloco na base de dados.
	 * @param bk Sala que será guardada
	 * @throws PersistenceExeception Problema na persistência de dados. 
	 */
	public void guardarBloco(Bloco bk) throws PersistenceExeception{
		BlocoDAO dao;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createBlocoDAO();
		dao.guardar(bk);
	}
	
	/**
	 * Exclui um objeto Bloco da base de dados. 
	 * @param sigla Sigla do Bloco a ser excluído.
	 * @throws PersistenceExeception Problema na persistência de dados.
	 */
	public void excluirBloco(String sigla) throws PersistenceExeception{
		BlocoDAO dao;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createBlocoDAO();
		dao.excluir(sigla);
	}
	
	/**
	 * Verifica se um determinado objeto Bloco existe na base de dados.
	 * @param bk Bloco à ser verificado.
	 * @return Retorna verdadeiro caso o bloco exista, caso contrário false.
	 * @throws PersistenceExeception Problema no acesso aos dados.
	 */
	public boolean contain(Bloco bk) throws PersistenceExeception{
		BlocoDAO dao;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createBlocoDAO();
		return dao.exist(bk.getSigla());
		
	}
	
	/**
	 * Localiza um determinado Bloco na base de dados.
	 * @param sigla Sigla do Bloco que deve ser localizado.
	 * @return O objeto Bloco caso esse seja encontrado ou null caso não existe bloco.
	 * @throws PersistenceExeception Erro no acesso aos dados.
         * @throws SalaInvalida Uma das salas deste bloco possui um estado inválido.
	 */
	public Bloco localiza(String sigla) throws PersistenceExeception, SalaInvalida{
            BlocoDAO dao;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createBlocoDAO();
            return dao.localiza(sigla);
	}
        
        /**
	 * Lista todos os objetos Bloco existentes na base de dados.
	 * @return Collection com todos os objetos Bloco da base de dados.
	 * @throws PersistenceExeception Problema no acesso aos dados.
         * @throws SalaInvalida Uma das salas deste bloco possui um estado inválido.
	 */
        public Collection<Bloco> listar() throws PersistenceExeception, SalaInvalida{
            BlocoDAO dao;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createBlocoDAO();
            return dao.listar();
        }
        
        /**
         * Atualiza dados de um Bloco.
         * @param bk Bloco a ser atualizado (sigla) e seus dados.
         * @throws PersistenceExeception Erro de acesso a base de dados.
         */
        public void update(Bloco bk) throws PersistenceExeception{
            BlocoDAO dao;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createBlocoDAO();
            dao.update(bk);
        }
    
}
