package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import roomaloc.sistema.Professor;
import roomaloc.sistema.dao.DAOFactory;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.PersistenceFactory;
import roomaloc.sistema.dao.ProfessorDAO;

/**
 * Classe que controla as ações de persistencia de objetos Professor na base de dados.
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorProfessor {
    private final int tipo = PersistenceFactory.BANCO_DE_DADOS;
    
    /**
     * Salva um novo Professor na base de dados.
     * @param prof Assistente que deve ser salvo.
     * @throws PersistenceExeception Erro de acesso a base de dadaos.
     */
    public void guardar(Professor prof) throws PersistenceExeception{
        ProfessorDAO dao = null;
	DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
	dao = daof.createProfDAO();
	dao.guardar(prof);
    }
    
    /**
	 * Exclui um Professor da base de dados.
	 * @param login Login do Professor que será excluido.
	 * @throws PersistenceExeception Erro no acesso aos dados ou exclusão de um Professor
	 * que não existe
	 */
	public void excluir(String login) throws PersistenceExeception{
            ProfessorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createProfDAO();
            dao.excluir(login);

	}
	
	/**
	 * Responde se um determinado Professor já está na Base de dados.
	 * @param prof Professor para a confirmação.
	 * @return true caso esteja, false caso contrario.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public boolean contain(Professor prof) throws PersistenceExeception{
            ProfessorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createProfDAO();
            return dao.exist(prof.getLogin());
	}
	
	/**
	 * Localiza e retorna o Professor com o login especificado
	 * @param login Login do Professor a ser localizado.
	 * @return Professor localizado, null caso não exista
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public Professor localiza(String login) throws PersistenceExeception{
            ProfessorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createProfDAO();
            return dao.localiza(login);
	}
        
        /**
         * Lista todos os Professor atualmente na base de dados.
         * @return Coleção com todos os Professor. 
         * @throws PersistenceExeception  Erro no acesso aos dados.
         */
        public Collection<Professor> listar() throws PersistenceExeception{
            ProfessorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createProfDAO();
            return dao.listar();
        }
}
