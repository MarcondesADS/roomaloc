package roomaloc.sistema.gerenciamento;

import java.util.Collection;
import roomaloc.sistema.AssistenteSala;
import roomaloc.sistema.dao.AssistenteSalaDAO;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.DAOFactory;
import roomaloc.sistema.dao.PersistenceFactory;

/**
 * Classe de gerenciamento de persistencia de objetos AssistenteSala na base de dados.
 * @author José Marcondes do Nascimento Junior
 */
public class GerenciadorAssistenteSala {
    private final int tipo = PersistenceFactory.BANCO_DE_DADOS;
    
    /**
     * Salva um novo Assistente na base de dados.
     * @param ass Assistente que deve ser salvo.
     * @throws PersistenceExeception Erro de acesso a base de dadaos.
     */
    public void guardar(AssistenteSala ass) throws PersistenceExeception{
        AssistenteSalaDAO dao = null;
	DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
	dao = daof.createAsSDAO();
	dao.guardar(ass);
    }
    
    /**
	 * Exclui um AssistenteSala da base de dados.
	 * @param login Login do AssistenteSala que será excluido.
	 * @throws PersistenceExeception Erro no acesso aos dados ou exclusão de um AssistenteSala
	 * que não existe
	 */
	public void excluir(String login) throws PersistenceExeception{
            AssistenteSalaDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createAsSDAO();
            dao.excluir(login);

	}
	
	/**
	 * Responde se um determinado AssistenteSala já está na Base de dados.
	 * @param ass AssistenteSala para a confirmação.
	 * @return true caso esteja, false caso contrario.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public boolean contain(AssistenteSala ass) throws PersistenceExeception{
		AssistenteSalaDAO dao = null;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createAsSDAO();
		return dao.exist(ass.getLogin());
	}
	
	/**
	 * Localiza e retorna o AssistenteSala com o login especificado
	 * @param login Login do AssistenteSala a ser localizado.
	 * @return AssistenteSala localizado, null caso não exista
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public AssistenteSala localiza(String login) throws PersistenceExeception{
		AssistenteSalaDAO dao = null;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createAsSDAO();
		return dao.localiza(login);
	}
        
        /**
         * Lista todos os AssistenteSala atualmente na base de dados.
         * @return Coleção com todos os AssistenteSala. 
         * @throws PersistenceExeception  Erro no acesso aos dados.
         */
        public Collection<AssistenteSala> listar() throws PersistenceExeception{
            AssistenteSalaDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createAsSDAO();
            return dao.listar();
        }
    
}
