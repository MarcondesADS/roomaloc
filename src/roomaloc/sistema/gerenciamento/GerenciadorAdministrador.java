package roomaloc.sistema.gerenciamento;


import java.util.Collection;
import roomaloc.sistema.Administrador;
import roomaloc.sistema.dao.AdministradorDAO;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.dao.DAOFactory;
import roomaloc.sistema.dao.PersistenceFactory;

/**
 * Classe de acesso de opções de permanência de objetos Administrador.
 * @author José Marcondes do Nascimento Junior.
 *
 */
public class GerenciadorAdministrador {

	private int tipo = PersistenceFactory.BANCO_DE_DADOS;
	
	/**
	 * Salva um novo administrador na base de dados.
	 * @param adm Novo Administrador
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public void guardarAdm(Administrador adm) throws PersistenceExeception{
		AdministradorDAO dao = null;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createAdmDAO();
		dao.guardar(adm);
	}
	
	/**
	 * Exclui um administrador da base de dados.
	 * @param login Login do administrador que será excluido.
	 * @throws PersistenceExeception Erro no acesso aos dados ou exclusão de um admimistrador
	 * que não existe
	 */
	public void excluirAdm(String login) throws PersistenceExeception{
		AdministradorDAO dao = null;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createAdmDAO();
		dao.excluir(login);
	}
	
	/**
	 * Responde se um determinado Administrador já está na Base de dados.
	 * @param adm Administrador para a confirmação.
	 * @return true caso esteja, false caso contrario.
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public boolean contain(Administrador adm) throws PersistenceExeception{
		AdministradorDAO dao = null;
		DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
		dao = daof.createAdmDAO();
		return dao.exist(adm.getLogin());
		
	}
	
	/**
	 * Localiza e retorna o administrador com o login especificado
	 * @param login Login do administrador a ser localizado.
	 * @return Administrador localizado, null caso não exista
	 * @throws PersistenceExeception Erro no acesso aos dados.
	 */
	public Administrador localiza(String login) throws PersistenceExeception{
            AdministradorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createAdmDAO();
            return dao.localiza(login);
	}
        
        /**
         * Lista todos os administradores atualmente na base de dados.
         * @return Coleção com todos os administradores. 
         * @throws PersistenceExeception  Erro no acesso aos dados.
         */
        public Collection<Administrador> listar() throws PersistenceExeception{
            AdministradorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createAdmDAO();
            return dao.listar();
        }
        
        /**
         * Atualiza os dados do administrador na base de dados.
         * @param adm Administrador a ser atualizado(login) e  seus novos dados.
         * @throws PersistenceExeception Erro de acesso aos dados.
         */
        public void update(Administrador adm) throws PersistenceExeception{
            AdministradorDAO dao = null;
            DAOFactory daof = PersistenceFactory.criarDAOFactory(this.tipo);
            dao = daof.createAdmDAO();
            dao.update(adm);
        }
	
	
}
