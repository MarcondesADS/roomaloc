
package roomaloc.sistema;

/**
 * Definição de diferentes tipos de salas.
 * @author José Marcondes do Nascimento Junior
 */
public enum TipoSala {
    NORMAL, INTELIGENTE, LABORATORIO
}
