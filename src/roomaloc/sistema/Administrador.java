package roomaloc.sistema;

import java.util.ArrayList;
import java.util.Collection;
import roomaloc.sistema.dao.PersistenceExeception;
import roomaloc.sistema.gerenciamento.GerenciadorAdministrador;
import roomaloc.sistema.gerenciamento.GerenciadorAssistenteSala;
import roomaloc.sistema.gerenciamento.GerenciadorProfessor;

/**
 * Classe de ações de administradores.
 * @author José Marcondes do Nascimento Junior
 */
public class Administrador extends Usuario{

	/**
	 * Constrói um objeto administrador com o login, senha e matrícula passado.
	 * @param login login do usuário.
	 * @param senha senha do usuário.
	 * @param matricula matricula do usuário.
	 */
	public Administrador(String login, String senha, String matricula) {
		this(login, senha, matricula, null);
	}
        
        /**
	 * Constrói um objeto administrador com o login, senha, matrícula, e unidade academica passado.
	 * @param login Login do usuário.
	 * @param senha Senha do usuário.
	 * @param matricula Matricula do usuário.
         * @param undAcad Unidade acadêmica do usuário.
	 */
        public Administrador(String login, String senha, String matricula, String undAcad) {
		super(login, senha, matricula,undAcad);		
	}
	
        /**
         * Cadastra um novo administrador no sistema.
         * @param usr Novo professor
         * @throws PersistenceExeception Erro no acesso aos dados ou tentativa de cadastro
         * duplo.
         */
	private void cadastrarUsuario(Administrador usr) throws PersistenceExeception{
            GerenciadorAdministrador gAdm = new  GerenciadorAdministrador();
            gAdm.guardarAdm(usr);
            
	}
        
        /**
         * Cadastra um novo assistente de sala no sistema.
         * @param usr Novo professor
         * @throws PersistenceExeception Erro no acesso aos dados ou tentativa de cadastro
         * duplo.
         */
        private void cadastrarUsuario(AssistenteSala usr) throws PersistenceExeception{
            GerenciadorAssistenteSala gAss = new GerenciadorAssistenteSala();
            gAss.guardar(usr);
	}
        
        /**
         * Cadastra um novo professor no sistema.
         * @param usr Novo professor
         * @throws PersistenceExeception Erro no acesso aos dados ou tentativa de cadastro
         * duplo.
         */
        private void cadastrarUsuario(Professor usr) throws PersistenceExeception{
            GerenciadorProfessor gProf = new GerenciadorProfessor();
            gProf.guardar(usr);
	}
	
        /**
         * Exclui um Usuario do sistema.
         * @param login Usuário à ser excluido.
         * @throws PersistenceExeception Erro no acesso aos dados.
         * @throws PermissaoException Erro de permição, lançado quando um administrador
         * tenta excluir outro administrador.
         */
        @Override
	public void excluirUsuario(String login) throws PersistenceExeception, PermissaoException{
            GerenciadorAdministrador gAdm = new  GerenciadorAdministrador();
            Administrador adm = gAdm.localiza(login);
            if(adm != null){
                if(adm.equals(this)){
                    gAdm.excluirAdm(login);
                }else{
                    throw new PermissaoException("Administradores só podem ser excluidos por eles mesmos");
                }
            }else{
                GerenciadorAssistenteSala gAss = new  GerenciadorAssistenteSala();
                AssistenteSala ass = gAss.localiza(login);
                if(ass != null){
                    gAss.excluir(login);
                }
                else{
                    GerenciadorProfessor gProf = new GerenciadorProfessor();
                    gProf.excluir(login);
                }
            }
	}
        /**
         * {@inheritDoc }
         */
        @Override
        public void cadastrarUsuario(Usuario usr) throws PersistenceExeception, PermissaoException{
            
            if(usr instanceof Administrador)
                cadastrarUsuario((Administrador) usr);
            else if(usr instanceof AssistenteSala)
                cadastrarUsuario((AssistenteSala) usr);
            else
                cadastrarUsuario((Professor) usr);
        }
        
        /**
         * Retorna uma lista como todos os usuário que o administrador tem acesso;
         * @return Coleção com todos os usuários.
         * @throws PersistenceExeception Erro de acesso aos dados
         */
        @Override
        public Collection<Usuario> listarUsuarios() throws PersistenceExeception{
            GerenciadorAdministrador gAdm = new GerenciadorAdministrador();
            GerenciadorAssistenteSala gAsS = new GerenciadorAssistenteSala();
            GerenciadorProfessor gProf = new GerenciadorProfessor();
            Collection<Usuario> list = new ArrayList<>();
            try{
            list.addAll(gAdm.listar());
            }catch(NullPointerException ex){}
            try{
            list.addAll(gAsS.listar());
            }catch(NullPointerException ex){}
            try{
            list.addAll(gProf.listar());
            }catch(NullPointerException ex){}
            return list;
        }

        @Override
    public void setSenha(String senha) throws PersistenceExeception{
        super.setSenha(senha);
        if(senha.length() < 6)
            throw new PersistenceExeception("Senhas devem ter no minimo 6 caracteres");
        GerenciadorAdministrador ga = new GerenciadorAdministrador();
        ga.update(this);
    }
        
        

}
