package roomaloc.sistema;

/**
 * Classe de ações de Professor.
 * @author José Marcondes do Nascimento Junior
 */
public class Professor extends Usuario{

        /**
         * Cria um professor contento o login, senha e matricula passados.
         * @param login Login do professor.
         * @param senha Senha do professor.
         * @param matricula Matricula do professor.
         */
	public Professor(String login, String senha, String matricula) {
		super(login, senha, matricula);
	}
 

        /**
         * Cria um professor contento o login, senha, matricula e unidade academica passados.
         * @param login Login do professor.
         * @param senha Senha do professor.
         * @param matricula Matricula do professor.
         * @param und Unidade academica do professor.
         */
        public Professor(String login, String senha, String matricula,String und) {
            super(login, senha, matricula, und);
        }
}
