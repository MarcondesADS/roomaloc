package roomaloc.sistema;

public class PermissaoException extends Exception {
	public PermissaoException(){
		super();
	}
	public PermissaoException(String msg){
		super(msg);
	}
}
