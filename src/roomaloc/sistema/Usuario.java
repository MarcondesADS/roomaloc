package roomaloc.sistema;

import roomaloc.sistema.dao.PersistenceExeception;

public abstract class Usuario implements Gerenciador{
	/**
     * Login do usuário que deve ser único.
     */
    private String login;
    
    /**
     * Senha do usuário.
     */
    private String senha;
    
    /**
     * Matrícula do usuário.
     */
    private String matricula;
    
    /**
     * Unidade acadêmica do usuário, este atributo é opcional.
     */
    private String  undAcademica;
    
    protected Usuario(String login, String senha, String matricula){
        this(login, senha, matricula, null);
    }
    
    protected Usuario(String login, String senha, String matricula, String unidade){
        this.login = login;
        this.senha = senha;
        this.matricula = matricula;
        this.undAcademica = unidade;
    }

    @Override
    public boolean equals(Object obj) {
    	if(obj instanceof Usuario){
    		Usuario usr = (Usuario) obj;
    		return usr.getLogin().equals(this.login);
    	}
    	return false;
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) throws PersistenceExeception{
        this.senha = senha;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getUndAcademica() {
        return undAcademica;
    }

    public void setUndAcademica(String undCademica) {
        this.undAcademica = undCademica;
    }
}
