package roomaloc.config;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;
import roomaloc.sistema.dao.daobd.ConnectionManager;

/**
 * Classe para configuraçoes gerais do sistema.
 * @author José Marcondes do Nascimento Junior
 */
public abstract class Configuracao {
    
    public static void initConnectionConfig(){
            DocumentBuilderFactory docBuildFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            try {
                docBuilder = docBuildFac.newDocumentBuilder();
                Document docXml = docBuilder.parse(Configuracao.class.getResource("config.xml").toString());
                Node bdConfig = docXml.getElementsByTagName("bdConfig").item(0);
                Element elementBdConfig = (Element) bdConfig;
                String url = ((Element) elementBdConfig.getElementsByTagName("url").item(0)).getTextContent();
                String nome = ((Element) elementBdConfig.getElementsByTagName("nome").item(0)).getTextContent();
                String senha = ((Element) elementBdConfig.getElementsByTagName("senha").item(0)).getTextContent();
                ConnectionManager.setUrl(url);
                ConnectionManager.setNome(nome);
                ConnectionManager.setSenha(senha);
                
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            }
    }
    
    public static void startScript(){
            DocumentBuilderFactory docBuildFac = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder;
            try {
                docBuilder = docBuildFac.newDocumentBuilder();
                Document docXml = docBuilder.parse(Configuracao.class.getResource("config.xml").toString());
                Node bdConfig = docXml.getElementsByTagName("bdConfig").item(0);
                Element elementBdConfig = (Element) bdConfig;
                String script = ((Element) elementBdConfig.getElementsByTagName("script").item(0)).getTextContent();
                Statement st = ConnectionManager.pegarConnection().createStatement();
                st.executeUpdate(script);
                st.close();
            } catch (ParserConfigurationException | SAXException | IOException ex) {
                Logger.getLogger(ConnectionManager.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
        }
    }
    
    
}
