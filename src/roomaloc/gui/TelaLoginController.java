/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.gui;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javax.security.auth.login.AccountException;
import javax.swing.JOptionPane;
import roomaloc.sistema.Administrador;
import roomaloc.sistema.Sistema;
import roomaloc.sistema.Usuario;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * FXML Controller class
 *
 * @author José Marcondes do Nascimento Junior
 */
public class TelaLoginController implements Initializable {

    @FXML
    private TextField login;
    @FXML
    private TextField senha;
    @FXML
    private Button ButtonEntrar;
    
    @FXML
    public void BEntrarActionHandler(ActionEvent event){
        try{
        Sistema sis = Sistema.getInstance();
        sis.login(login.getText(), senha.getText());
        if(((Usuario) sis.getUsrLogado()).getLogin().equals("adm") &&
                ((Usuario) sis.getUsrLogado()).getSenha().equals("adm")){
            String newSenha = JOptionPane.showInputDialog("Digite a nova senha");
            ((Administrador) sis.getUsrLogado()).setSenha(newSenha);
        }
        TelaLogin.getStage().close();
        new TelaInicial().start(new Stage());
        }catch(PersistenceExeception pEx){
            JOptionPane.showMessageDialog(null, pEx.getMessage(),"Erro",JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TelaLoginController.class.getName()).log(Level.SEVERE, null,pEx);
        }catch(NullPointerException ex){
            JOptionPane.showMessageDialog(null, "Usuario inexistente, por favor insira uma conta válida");
        }catch(AccountException ex){
            JOptionPane.showMessageDialog(null, "Senha incorreta, por favor digite novamente");
        }
    }
    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
