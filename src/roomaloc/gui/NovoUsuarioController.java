/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableArray;
import javafx.collections.ObservableListBase;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DialogPane;
import javafx.scene.control.TextField;
import javax.swing.JOptionPane;
import roomaloc.sistema.Administrador;
import roomaloc.sistema.AssistenteSala;
import roomaloc.sistema.PermissaoException;
import roomaloc.sistema.Professor;
import roomaloc.sistema.Sistema;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * FXML Controller class
 *
 * @author José Marcondes do Nascimento Junior
 */
public class NovoUsuarioController implements Initializable {

    
     @FXML
    private TextField loginField;

    @FXML
    private TextField senhaField;

    @FXML
    private TextField matriculaField;

    @FXML
    private TextField unidadeAcadField;

    @FXML
    private ChoiceBox<String> tipoChoice;

    @FXML
    void AdicionarActionHandler(ActionEvent event) {
        String login = loginField.getText();
        String senha = senhaField.getText();
        String matricula = matriculaField.getText();
        String unidadeAcad = unidadeAcadField.getText();
        try{
            switch(tipoChoice.getValue()){
                case "Administrador":Sistema.getInstance().cadastrarUsuario(new Administrador(login, senha, matricula, unidadeAcad));
                    break;
                case "Assistente de Sala":Sistema.getInstance().cadastrarUsuario(new AssistenteSala(login, senha, matricula, unidadeAcad));
                    break;
                case "Professor":Sistema.getInstance().cadastrarUsuario(new Professor(login, senha, matricula, unidadeAcad));
            }
        JOptionPane.showMessageDialog(null, "Usuario cadastrado com sucesso!");
        loginField.setText("");
        senhaField.setText("");
        matriculaField.setText("");
        unidadeAcadField.setText("");
        }catch(PermissaoException ex){
            JOptionPane.showMessageDialog(null, "Você não tem permição para fazer isso!","Alerta",JOptionPane.WARNING_MESSAGE);
        }catch(PersistenceExeception ex){
            Logger.getLogger(NovoUsuarioController.class.getName()).log(Level.SEVERE,null,ex);
            JOptionPane.showMessageDialog(null, ex.getMessage(),
                    "Erro", JOptionPane.ERROR_MESSAGE);
        }
    }

    @FXML
    void LimparActionHandler(ActionEvent event) {
        loginField.setText("");
        senhaField.setText("");
        matriculaField.setText("");
        unidadeAcadField.setText("");
    }

    @FXML
    void VoltarActionHandler(ActionEvent event) {
         try {
             TelaInicial.getScene().setRoot(FXMLLoader.load(getClass().getResource("TelaInicial.fxml")));
         } catch (IOException ex) {
             JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contecte um administrador");
             Logger.getLogger(NovoUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
         }
    }    

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tipoChoice.setItems(FXCollections.observableArrayList("Administrador","Assistente de Sala","Professor"));
        tipoChoice.setValue("Administrador");
    }
    
}
