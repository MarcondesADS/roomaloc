/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.PermissaoException;
import roomaloc.sistema.Sistema;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * FXML Controller class
 *
 * @author José Marcondes do Nascimento Junior
 */
public class NovoBlocoController implements Initializable {
    @FXML
    private TextField nomeTF;
    @FXML
    private TextField SiglaTF;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void confirmarActionHandler(ActionEvent event) throws PermissaoException, PersistenceExeception {
        Sistema.getInstance().cadastrarBloco(new Bloco(nomeTF.getText()));
    }
    
}
