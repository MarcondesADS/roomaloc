/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package roomaloc.gui;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

/**
 *
 * @author José Marcondes do Nascimento Junior
 */
public class TelaInicial extends Application {
    
    private static Scene scene;
    private static Stage stage;
    
    @Override
    public void start(Stage primaryStage) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("TelaInicial.fxml"));
        
        scene = new Scene(root);
        stage = primaryStage;
        primaryStage.setTitle("Bem-vindo ao RoomAloc!!!");
        primaryStage.setScene(scene);
        scene.getStylesheets().add(getClass().getResource("Theme.css").toString());
        primaryStage.show();
        } catch (IOException ex) {
            Logger.getLogger(TelaInicial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static Scene getScene() {
        return scene;
    }

    public static void setScene(Scene scene) {
        TelaInicial.scene = scene;
    }

    public static Stage getStage() {
        return stage;
    }

    public static void setStage(Stage stage) {
        TelaInicial.stage = stage;
    }
    
    
    
}
