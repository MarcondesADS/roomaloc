package roomaloc.gui;

import java.io.IOException;
import java.net.URL;
import java.time.Clock;
import java.util.Collection;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import roomaloc.sistema.PermissaoException;
import roomaloc.sistema.Sistema;
import roomaloc.sistema.Usuario;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * FXML Controller class
 *
 * @author José Marcondes do Nascimento Junior
 */
public class TelaDeletarUsuarioController implements Initializable {

    @FXML
    private TableView<Usuario> tableUsuarios;

    @FXML
    private TableColumn<Usuario, String> loginColumn;

    @FXML
    private TableColumn<Usuario, String> matriculaColumn;

    @FXML
    private TableColumn<Usuario, String> unidadeColumn;

    @FXML
    void deletarActionHandler(ActionEvent event) {
        try{
            if(JOptionPane.showConfirmDialog(null, "Tem certeza que deseja excluir?","Excluir", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE)== JOptionPane.YES_OPTION){
                Usuario usr = tableUsuarios.getSelectionModel().getSelectedItem();           
                Sistema.getInstance().excluirUsuario(usr.getLogin());
                if(Sistema.getInstance().getUsrLogado() == null){
                    TelaInicial.getStage().close();
                    new TelaLogin().start(new Stage());
                }
                else
                    tableUsuarios.getItems().remove(usr);
            }
        }catch(PermissaoException ex){
            JOptionPane.showMessageDialog(null, "Você não tem permissão para realizar essa ação: "+ex.getMessage());
        }catch(PersistenceExeception ex){
            Logger.getLogger(TelaDeletarUsuarioController.class.getName()).log(Level.SEVERE,null,ex);
            JOptionPane.showMessageDialog(null, "Ocorreu um grave erro no sistema, contacte um administrador");
        } catch (Exception ex) {
            Logger.getLogger(TelaDeletarUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    @FXML
    void voltarActionHandler(ActionEvent event) {
        try {
             TelaInicial.getScene().setRoot(FXMLLoader.load(getClass().getResource("TelaInicial.fxml")));
         } catch (IOException ex) {
             JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contecte um administrador");
             Logger.getLogger(NovoUsuarioController.class.getName()).log(Level.SEVERE, null, ex);
         }
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        loginColumn.setCellValueFactory(new PropertyValueFactory<>("login"));
        matriculaColumn.setCellValueFactory(new PropertyValueFactory<>("matricula"));
        unidadeColumn.setCellValueFactory(new PropertyValueFactory<>("undAcademica"));
        tableUsuarios.setItems(FXCollections.observableArrayList(Sistema.getInstance().listarUsuarios()));
    }
}
