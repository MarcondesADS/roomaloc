
package roomaloc.gui;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javax.swing.JOptionPane;
import roomaloc.sistema.Sistema;

/**
 * FXML Controller class
 *
 * @author José Marcondes do Nascimento Junior
 */
public class TelaInicialController implements Initializable {
    @FXML
    private Button cNovoUsr;
    @FXML
    private Button exUsr;

    
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void CadastrarUsrActionHandler(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("NovoUsuario.fxml"));
            TelaInicial.getScene().setRoot(root);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contacte um administrador",
                    "Erro",JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
     @FXML
    void DeletarUsrActionHandler(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("TelaDeletarUsuario.fxml"));
            TelaInicial.getScene().setRoot(root);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contacte um administrador",
                    "Erro",JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void logoffActionHandler(ActionEvent event) {
        Sistema.getInstance().logoff();
        TelaInicial.getStage().close();
        try {
            new TelaLogin().start(new Stage());
        } catch (Exception ex) {
            Logger.getLogger(TelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contacte um administrador",
                    "Erro",JOptionPane.ERROR_MESSAGE);
        }
    }

    @FXML
    private void cadastrarBkActionHandler(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("NovoBloco.fxml"));
            TelaInicial.getScene().setRoot(root);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contacte um administrador",
                    "Erro",JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void excluirBKActionHandler(ActionEvent event) {
        try {
            Parent root = FXMLLoader.load(getClass().getResource("ExcluirBloco.fxml"));
            TelaInicial.getScene().setRoot(root);
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Ocorreu um erro grave no sistema, contacte um administrador",
                    "Erro",JOptionPane.ERROR_MESSAGE);
            Logger.getLogger(TelaInicialController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
}
