package roomaloc.gui;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextInputDialog;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;
import roomaloc.sistema.Bloco;
import roomaloc.sistema.PermissaoException;
import roomaloc.sistema.SalaInvalida;
import roomaloc.sistema.Sistema;
import roomaloc.sistema.dao.PersistenceExeception;

/**
 * FXML Controller class
 *
 * @author José Marcondes do Nascimento Junior
 */
public class ExcluirBlocoController implements Initializable {
    @FXML
    private TableView<Bloco> bksTable;
    @FXML
    private TableColumn<Bloco, String> siglaClm;
    @FXML
    private TableColumn<Bloco, String> nomeClm;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            siglaClm.setCellValueFactory(new PropertyValueFactory<>("sigla"));
            nomeClm.setCellValueFactory(new PropertyValueFactory<>("nome"));
            bksTable.setItems(FXCollections.observableArrayList(Sistema.getInstance().listarBlocos()));
        } catch (PermissaoException ex) {
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersistenceExeception ex) {
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SalaInvalida ex) {
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void excluirActionHandler(ActionEvent event) {
        try{
            if(JOptionPane.showConfirmDialog(null, "Tem certeza que deseja excluir?","Excluir", JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE)== JOptionPane.YES_OPTION){
                Bloco bk = bksTable.getSelectionModel().getSelectedItem();           
                Sistema.getInstance().excluirBloco(bk.getSigla());
                bksTable.getItems().remove(bk);
            }
        }catch(PermissaoException ex){
            JOptionPane.showMessageDialog(null, "Você não tem permissão para realizar essa ação: "+ex.getMessage());
        }catch(PersistenceExeception ex){
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE,null,ex);
            JOptionPane.showMessageDialog(null, "Ocorreu um grave erro no sistema, contacte um administrador");
        } catch (Exception ex) {
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void editarActionHandler(ActionEvent event) {
        try {
            TextInputDialog a = new TextInputDialog("Digite o novo nome");
            String nome = a.showAndWait().get();
            Bloco bk = bksTable.getSelectionModel().getSelectedItem();
            bk.setNome(nome);
            Sistema.getInstance().atualizarBloco(bk);
        } catch (PermissaoException ex) {
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (PersistenceExeception ex) {
            Logger.getLogger(ExcluirBlocoController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void voltar(ActionEvent event) {
    }
    
}
